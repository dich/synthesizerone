# SynthesizerOne

A software Synthesizer.

## How To

- Download Juce from http://www.juce.com/ and setup it following instructions depending on your system

- clone the stk_module project from git in the modules folder (https://github.com/danlin/stk_module.git)

- Clone this project

- Open IntroJucer and load the SynthesizerOne.jucer file.

- Check the paths of the modules

- Compile it (depending of your system)

## Linux/Ubuntu

You need the standard developer tools and some other libraries:

- Ubuntu Make (https://wiki.ubuntu.com/ubuntu-make)

	sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
	sudo apt-get update
	sudo apt-get install ubuntu-make

- (optional, in case you get error) libxcursor

	sudo apt-get install libxcursor-dev

- (optional, in case you get error) libxinerama

	sudo apt-get install libxinerama-dev

## Windows 7

- download and install windows sdk 7

http://www.microsoft.com/en-us/download/details.aspx?id=3138

- download and install Visual Studio Express 2012

https://www.microsoft.com/en-US/download/details.aspx?id=34673

## MAC OSX

todo