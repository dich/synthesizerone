/*
  ==============================================================================

    Mixer.h
    Created: 9 Jun 2015 5:34:25pm
    Author:  daniele

  ==============================================================================
*/

#ifndef MIXER_H_INCLUDED
#define MIXER_H_INCLUDED

#include "JuceHeader.h"

using namespace stk;

class Mixer
{
private:
    StkFloat _signal1;
    StkFloat _signal2;
    StkFloat _signal3;
    StkFloat _signal4;
    float _vol1;
    float _vol2;
    float _vol3;
    float _vol4;
public:
    Mixer();
    void SetVolumes(int cc1, int cc2, int cc3, int cc4);
    StkFloat output();
    void input(StkFloat signal1, StkFloat signal2, StkFloat signal3, StkFloat signal4);
};





#endif  // MIXER_H_INCLUDED
