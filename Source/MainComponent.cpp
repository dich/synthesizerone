/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

#include "OscillatorComponent.h"
#include "MixerComponent.h"

using namespace stk;

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent: public Component, public AudioIODeviceCallback
{
public:
    //==============================================================================
    MainContentComponent()
    {
        //ApplicationProperties *appProperties = new ApplicationProperties();
        //PropertiesFile* userSettings = appProperties->getUserSettings();
        //ScopedPointer<XmlElement> savedAudioState(userSettings->getXmlValue("audioDeviceState"));
        
        //deviceManager.initialiseWithDefaultDevices(2, 2, savedAudioState, true);
        //deviceManager.initialise(2, 2, savedAudioState, true);
	    deviceManager.addAudioCallback(this);
	    setSize (720, 400);
	    
	    deviceSelector = new AudioDeviceSelectorComponent (deviceManager, 0, 0, 2, 2, false, false, true, false);
	    addAndMakeVisible (deviceSelector);
	    deviceSelector->setBounds(290,0,440,400);
	    
        int h = 90;
        int w = 270;
        int top = 20;
        int left = 20;
        //setSize (h*3,w);
        
        c1 = new OscillatorComponent();
        c2 = new OscillatorComponent();
        mx = new MixerComponent();
        
        this->addAndMakeVisible(c1,-1);
        this->addAndMakeVisible(c2,-1);
        this->addAndMakeVisible(mx,-1);
        
        c1->setBounds(left,top,w,h);
        c2->setBounds(left,top+h,w,h);
        mx->setBounds(left,top+(h*2),w,h);
    }

    ~MainContentComponent()
    {
        deviceSelector = nullptr;
    }

    void audioDeviceAboutToStart (AudioIODevice* device) {
	    Stk::setSampleRate(device->getCurrentSampleRate());
    }

    void audioDeviceStopped() {
    }

    void audioDeviceIOCallback (const float** inputChannelData, int numInputChannels,
                                                       float** outputChannelData, int numOutputChannels, int numSamples) {
        for (int i = 0; i < numSamples; ++i) {
            mx->GetMixer()->input(c1->GetOscillator()->output(),c2->GetOscillator()->output(),0,0);
		    StkFloat data = mx->GetMixer()->output();
            for (int chan = 0; chan < numOutputChannels; ++chan) {
			    outputChannelData[chan][i] = (float)data;
            }
	    }
    }

    //=======================================================================
    void paint (Graphics& g) override
    {
        // (Our component is opaque, so we must completely fill the background with a solid colour)
        g.fillAll (Colour (0xffffffff));
        // You can add your drawing code here!
    }

    void resized() override
    {
        // This is called when the MainContentComponent is resized.
        // If you add any child components, this is where you should
        // update their positions.
    }


private:
    //==============================================================================

    // Your private member variables go here...
    OscillatorComponent *c1;
    OscillatorComponent *c2;
    MixerComponent *mx;
    
    AudioDeviceManager deviceManager;
	ScopedPointer<AudioDeviceSelectorComponent> deviceSelector;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


// (This function is called by the app startup code to create our main component)
Component* createMainContentComponent()     { return new MainContentComponent(); }


#endif  // MAINCOMPONENT_H_INCLUDED
