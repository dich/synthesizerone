/*
  ==============================================================================

    Mixer.cpp
    Created: 9 Jun 2015 5:34:25pm
    Author:  daniele

  ==============================================================================
*/

#include "Mixer.h"

Mixer::Mixer()
{
    _signal1 = 0;
    _signal2 = 0;
    _signal3 = 0;
    _signal4 = 0;
    
    _vol1 = 0;
    _vol2 = 0;
    _vol3 = 0;
    _vol4 = 0;
}


StkFloat Mixer::output() {
    int count = 0;
    if (_signal1>0) count++;
    if (_signal2>0) count++;
    if (_signal3>0) count++;
    if (_signal4>0) count++;
    
    if (count==0) count = 1;
    
    return ((_signal1*_vol1)+
    (_signal2*_vol2)+
    (_signal3*_vol3)+
    (_signal4*_vol4))/count;
}

void Mixer::input(StkFloat signal1, StkFloat signal2, StkFloat signal3, StkFloat signal4) {
    _signal1 = signal1;
    _signal2 = signal2;
    _signal3 = signal3;
    _signal4 = signal4;
}

void Mixer::SetVolumes(int cc1, int cc2, int cc3, int cc4) {
    if (cc1>-1) {
        _vol1 = cc1/128.0;
    }

    if (cc2>-1) {
        _vol2 = cc2/128.0;
    }

    if (cc3>-1) {
        _vol3 = cc3/128.0;
    }
    
    if (cc4>-1) {
        _vol4 = cc4/128.0;
    }
}