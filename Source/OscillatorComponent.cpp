/*
  ==============================================================================

  This is an automatically generated GUI class created by the Introjucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Introjucer version: 3.1.0

  ------------------------------------------------------------------------------

  The Introjucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright 2004-13 by Raw Material Software Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "OscillatorComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
OscillatorComponent::OscillatorComponent ()
{
    addAndMakeVisible (label = new Label ("OCT",
                                          TRANS("OCT")));
    label->setFont (Font (15.00f, Font::plain));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (label2 = new Label ("new label",
                                           TRANS("FINE\n")));
    label2->setFont (Font (15.00f, Font::plain));
    label2->setJustificationType (Justification::centred);
    label2->setEditable (false, false, false);
    label2->setColour (TextEditor::textColourId, Colours::black);
    label2->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (sliderOct = new Slider ("new slider"));
    sliderOct->setRange (0, 127, 1);
    sliderOct->setSliderStyle (Slider::RotaryVerticalDrag);
    sliderOct->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sliderOct->addListener (this);

    addAndMakeVisible (label3 = new Label ("OCT",
                                           TRANS("SQR")));
    label3->setFont (Font (15.00f, Font::plain));
    label3->setJustificationType (Justification::centred);
    label3->setEditable (false, false, false);
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (label4 = new Label ("OCT",
                                           TRANS("SAW")));
    label4->setFont (Font (15.00f, Font::plain));
    label4->setJustificationType (Justification::centred);
    label4->setEditable (false, false, false);
    label4->setColour (TextEditor::textColourId, Colours::black);
    label4->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (label5 = new Label ("OCT",
                                           TRANS("SIN")));
    label5->setFont (Font (15.00f, Font::plain));
    label5->setJustificationType (Justification::centred);
    label5->setEditable (false, false, false);
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    addAndMakeVisible (sliderFine = new Slider ("new slider"));
    sliderFine->setRange (0, 127, 1);
    sliderFine->setSliderStyle (Slider::RotaryVerticalDrag);
    sliderFine->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sliderFine->addListener (this);

    addAndMakeVisible (sliderSin = new Slider ("new slider"));
    sliderSin->setRange (0, 127, 1);
    sliderSin->setSliderStyle (Slider::LinearVertical);
    sliderSin->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sliderSin->addListener (this);

    addAndMakeVisible (sliderSaw = new Slider ("new slider"));
    sliderSaw->setRange (0, 127, 1);
    sliderSaw->setSliderStyle (Slider::LinearVertical);
    sliderSaw->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sliderSaw->addListener (this);

    addAndMakeVisible (sliderSqr = new Slider ("new slider"));
    sliderSqr->setRange (0, 127, 1);
    sliderSqr->setSliderStyle (Slider::LinearVertical);
    sliderSqr->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    sliderSqr->addListener (this);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    _osc = new Oscillator();
    //[/Constructor]
}

OscillatorComponent::~OscillatorComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    label = nullptr;
    label2 = nullptr;
    sliderOct = nullptr;
    label3 = nullptr;
    label4 = nullptr;
    label5 = nullptr;
    sliderFine = nullptr;
    sliderSin = nullptr;
    sliderSaw = nullptr;
    sliderSqr = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void OscillatorComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colours::beige);

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void OscillatorComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    label->setBounds (7, 63, 64, 24);
    label2->setBounds (71, 63, 64, 24);
    sliderOct->setBounds (7, 7, 64, 56);
    label3->setBounds (215, 63, 41, 24);
    label4->setBounds (175, 63, 39, 24);
    label5->setBounds (135, 63, 39, 24);
    sliderFine->setBounds (71, 7, 64, 56);
    sliderSin->setBounds (143, 7, 23, 56);
    sliderSaw->setBounds (183, 7, 23, 56);
    sliderSqr->setBounds (223, 7, 23, 56);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void OscillatorComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == sliderOct)
    {
        //[UserSliderCode_sliderOct] -- add your slider handling code here..
        _osc->SetOctave(sliderThatWasMoved->getValue()/128*8);
        //[/UserSliderCode_sliderOct]
    }
    else if (sliderThatWasMoved == sliderFine)
    {
        //[UserSliderCode_sliderFine] -- add your slider handling code here..
        _osc->SetFine(sliderThatWasMoved->getValue());
        //[/UserSliderCode_sliderFine]
    }
    else if (sliderThatWasMoved == sliderSin)
    {
        //[UserSliderCode_sliderSin] -- add your slider handling code here..
        _osc->SetVolumes(sliderThatWasMoved->getValue(),-1,-1);
        //[/UserSliderCode_sliderSin]
    }
    else if (sliderThatWasMoved == sliderSaw)
    {
        //[UserSliderCode_sliderSaw] -- add your slider handling code here..
        _osc->SetVolumes(-1,sliderThatWasMoved->getValue(),-1);
        //[/UserSliderCode_sliderSaw]
    }
    else if (sliderThatWasMoved == sliderSqr)
    {
        //[UserSliderCode_sliderSqr] -- add your slider handling code here..
        _osc->SetVolumes(-1,-1,sliderThatWasMoved->getValue());
        //[/UserSliderCode_sliderSqr]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
Oscillator *OscillatorComponent::GetOscillator () {
    return _osc;
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Introjucer information section --

    This is where the Introjucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="OscillatorComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="fff5f5dc"/>
  <LABEL name="OCT" id="7bcd1adc1dbbb995" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="7 63 64 24" edTextCol="ff000000"
         edBkgCol="0" labelText="OCT" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="cf680986d30b8084" memberName="label2" virtualName=""
         explicitFocusOrder="0" pos="71 63 64 24" edTextCol="ff000000"
         edBkgCol="0" labelText="FINE&#10;" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <SLIDER name="new slider" id="c6d8162906cd9efa" memberName="sliderOct"
          virtualName="" explicitFocusOrder="0" pos="7 7 64 56" min="0"
          max="127" int="1" style="RotaryVerticalDrag" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <LABEL name="OCT" id="5a6fc374ae72479" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="215 63 41 24" edTextCol="ff000000"
         edBkgCol="0" labelText="SQR" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <LABEL name="OCT" id="3d7bc664f4faa17c" memberName="label4" virtualName=""
         explicitFocusOrder="0" pos="175 63 39 24" edTextCol="ff000000"
         edBkgCol="0" labelText="SAW" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <LABEL name="OCT" id="cad69a169170adbf" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="135 63 39 24" edTextCol="ff000000"
         edBkgCol="0" labelText="SIN" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="Default font" fontsize="15"
         bold="0" italic="0" justification="36"/>
  <SLIDER name="new slider" id="7bde955d991c1019" memberName="sliderFine"
          virtualName="" explicitFocusOrder="0" pos="71 7 64 56" min="0"
          max="127" int="1" style="RotaryVerticalDrag" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="3aad3bc380b83076" memberName="sliderSin"
          virtualName="" explicitFocusOrder="0" pos="143 7 23 56" min="0"
          max="127" int="1" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="d4bff7f1104001c6" memberName="sliderSaw"
          virtualName="" explicitFocusOrder="0" pos="183 7 23 56" min="0"
          max="127" int="1" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
  <SLIDER name="new slider" id="45071e6c99047bba" memberName="sliderSqr"
          virtualName="" explicitFocusOrder="0" pos="223 7 23 56" min="0"
          max="127" int="1" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]
