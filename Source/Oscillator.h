/*
  ==============================================================================

    Oscillator.h
    Created: 9 Jun 2015 2:17:00pm
    Author:  daniele

  ==============================================================================
*/

#ifndef OSCILLATOR_H_INCLUDED
#define OSCILLATOR_H_INCLUDED

#include "JuceHeader.h"

using namespace stk;

class Oscillator
{
private:
    SineWave *_sine;
    BlitSaw *_saw;
    BlitSquare *_square;

    int _octave;
    int _note;

    StkFloat _freq;
    StkFloat _fine;

    StkFloat _sineVol;
    StkFloat _sawVol;
    StkFloat _squareVol;

    void calcFreq();
public:
    Oscillator();
    StkFloat output();
    void input(StkFloat signal);

    void SetOctave(int octave);
    void SetFine(int cc);
    void SetVolumes(int ccSine, int ccSaw, int ccSquare);
};



#endif  // OSCILLATOR_H_INCLUDED
