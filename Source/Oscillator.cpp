/*
  ==============================================================================

    Oscillator.cpp
    Created: 9 Jun 2015 2:17:00pm
    Author:  daniele

  ==============================================================================
*/

#include "Oscillator.h"

Oscillator::Oscillator()
{
    _octave = 5;
    _note = 0;
    _fine = 0;

    _sineVol = 1.0;
    _sawVol = 0.0;
    _squareVol = 0.0;

    calcFreq();

    _sine = new SineWave();
    _saw = new BlitSaw();
    _square = new BlitSquare();
}

void Oscillator::calcFreq() {
    //http://newt.phys.unsw.edu.au/jw/notes.html
    int n = _octave*12+_note;
    _freq = pow(2.0,((n-60)/12))*261.6+_fine;

    //if 0 is A
    //_freq = pow(2.0,((n-69)/12))*440;
}


StkFloat Oscillator::output() {
    _sine->setFrequency(_freq);
    _saw->setFrequency(_freq);
    _square->setFrequency(_freq);

    /*
     * https://dsp.stackexchange.com/questions/3581/algorithms-to-mix-audio-signals-without-clipping
     * the best way to mix signals without clipping them seems to be this formula:
     * output = (sig1+sig2+sig3)/numberOfSignals
    */
    //return _sine->tick();
    return (_sine->tick()*_sineVol+_saw->tick()*_sawVol+_square->tick()*_squareVol)/3;
}

void Oscillator::input(StkFloat signal) {

}

void Oscillator::SetOctave(int octave) {
    _octave = octave;
    calcFreq();
}

void Oscillator::SetFine(int cc) {
    _fine = cc/128.0*96.0;
    calcFreq();
}


void Oscillator::SetVolumes(int ccSine, int ccSaw, int ccSquare) {
    if (ccSine>-1) {
        _sineVol = ccSine/128.0;
    }

    if (ccSaw>-1) {
        _sawVol = ccSaw/128.0;
    }

    if (ccSquare>-1) {
        _squareVol = ccSquare/128.0;
    }
}